# tasks


# Opis
- Każdy task należy wypchnąć oddzielnie poprzez stworzenie brancha ze zmianami następnie wypchnać do głównego 
repozytorium i stworzyć
- Proszę zrobić conajmniej 2 commit z jakimiś poprawki/literówki/dodatkowe zdanie 

1. [ ] Dodaj ignorowanie katalogu ".idea" przez gita
2. [ ] Dodaj ignorowanie katalogu "out" przez gita
3. [ ] Dodaj ignorowanie katalogu "build" przez gita
4. [ ] Dodaj ignorowanie katalogu "target" przez gita
5. [ ] Dodaj ignorowanie katalogu "tmp" przez gita
6. [ ] Dodaj ignorowanie katalogu ".bak" przez gita
7. [ ] Dodaj ignorowanie katalogu ".orig" przez gita

Każda osoba
1. [ ] Dodaj wpis o sobie w pliku [authors](authors.md)
2. [ ] Dodaj wpis o swoim projekcie w pliku [projects](projects.md)
3. [ ] Dodaj w [README](README.md) link do swojego posta


